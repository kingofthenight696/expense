import Vue from 'vue';
import VueRouter from 'vue-router';

import Expense from './pages/Expense';
import AddExpense from './pages/AddExpense';
import Compensation from './pages/Compensation';
import AddCompensation from './pages/AddCompensation';

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes:[
        {
            path:'/',
            name: 'expense',
            component: Expense,
            title: 'Expense',
        },
        {
            path:'/add-expense',
            name: 'add-expense',
            component: AddExpense,
            title: 'Add expense',
        },
        {
            path:'/compensation',
            name: 'compensation',
            component: Compensation,
            title: 'Compensation',
        },
        {
            path:'/add-compensation/:id',
            name: 'add-compensation',
            component: AddCompensation,
            title: 'Add compensation',
        },
    ]
});

export default router;

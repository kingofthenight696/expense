<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::group(['namespace' => 'API'], function () {

    Route::get('expense-types', 'ExpenseTypeController@index');
    Route::get('expense-categories', 'ExpenseCategoryController@index');
    Route::resource('companies', 'CompanyController');
    Route::resource('compensations', 'CompensationController');
    Route::resource('expense', 'ExpenseController');
    Route::resource('receipt', 'ReceiptController');

});

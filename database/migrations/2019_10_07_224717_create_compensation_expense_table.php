<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompensationExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compensation_expense', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('compensation_id')->nullable();
            $table->unsignedBigInteger('expense_id')->nullable();


            $table->foreign('expense_id')
                ->references('id')
                ->on('expenses')
                ->onDelete('cascade');

            $table->foreign('compensation_id')
                ->references('id')
                ->on('compensations')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compensation_expense');
        Schema::create('expenses', function (Blueprint $table) {
            $table->dropForeign(['expense_id', 'compensation_id']);
        });

    }
}

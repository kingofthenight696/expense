<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('expense_category_id')->nullable();
            $table->unsignedBigInteger('expense_type_id')->nullable();
            $table->date('date');
            $table->decimal('price');
            $table->text('comment');
            $table->string('note');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('project_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->timestamps();


            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('expense_category_id')
                ->references('id')
                ->on('expense_categories')
                ->onDelete('set null');

            $table->foreign('expense_type_id')
                ->references('id')
                ->on('expense_types')
                ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');

        Schema::create('expenses', function (Blueprint $table) {
            $table->dropForeign([
                'company_id',
                'project_id',
                'user_id',
                'expense_category_id',
                'expense_type_id'
            ]);
        });
    }
}

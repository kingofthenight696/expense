<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker, $attributes) {

    return [
        'company_id' => $attributes['company_id'],
        'name' => $faker->sentence,
    ];
});

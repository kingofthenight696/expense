<?php

use App\Models\Company;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Company::class, 3)->create()->each(function ($company) {
            factory(User::class, 3)->create([
                'company_id' => $company->id,
            ]);
            factory(Project::class, 3)->create([
                'company_id' => $company->id
            ]);
        });
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?? '',
            'date' => $this->date ?? '',
            'price' => $this->price ?? 0,
            'note' => $this->note ?? '',
            'comment' => $this->comment ?? '',
            'company' => $this->company ?? '',
            'project' => $this->project ?? '',
            'user' => $this->user ??  '',
            'expenseCategory' => $this->expenseCategory ?? '',
            'expenseType' => $this->expenseType ?? '',
        ];
    }
}

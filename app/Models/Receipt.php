<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $fillable = [
        'name',
        'original_name',
        'mime',
        'size',
        'expense_id',
        'compensation_id',
    ];

    protected $casts = [
        'size' => 'integer',
    ];

    public function expense()
    {
        return $this->belongsTo(Expense::class);
    }

    public function compensation()
    {
        return $this->belongsTo(compensation::class);
    }
}

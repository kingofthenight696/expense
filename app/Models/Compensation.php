<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Compensation extends Model
{
    protected $table = 'compensations';
    protected $fillable = [
        'date',
        'price',
        'comment',
        'company_id',
        'project_id',
        'user_id',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
        'price' => 'decimal:8',
    ];

    public function expenses()
    {
        return $this->belongsToMany('Expenses');
    }

    public function receipt()
    {
        return $this->hasMany(Receipt::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

}

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.4/installation#installation)


Clone the repository

    git clone https://kingofthenight696@bitbucket.org/kingofthenight696/expense.git

Switch to the repo folder

    cd  expense

Install all the dependencies using composer

    composer install
	
Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations and seeders (**Set the database connection in .env before migrating**)

    php artisan migrate:fresh --seed

Install all the dependencies using npm

    npm install

Create view file from npm

    npm run build
	
Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone https://kingofthenight696@bitbucket.org/kingofthenight696/expense.git
    cd expense
    composer install
    cp .env.example .env
    php artisan key:generate
	npm install
	npm run build
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate:fresh --seed
    php artisan serve

## Environment variables

- `.env` - Environment variables can be set in this file